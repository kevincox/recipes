import "./global.scss";

import App from "./App.svelte";

void navigator.serviceWorker.register(process.env.NODE_ENV == "development" ? "/sw.ts" : "/sw.js");

export default new App({
	target: document.body,
});
