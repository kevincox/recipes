let link: HTMLAnchorElement | undefined;

export function saveFile(name: string, blob: Blob): void {
	if (!link) {
		link = document.createElement("a");
		link.style.display = "none";
		document.body.append(link);
	}

	let url = URL.createObjectURL(blob);
	link.setAttribute("href", url);
	link.setAttribute("download", name);
	link.click();
	URL.revokeObjectURL(url);
}

export function saveText(name: string, text: string, type: string): void {
	saveFile(name, new Blob([text], {type}));
}
