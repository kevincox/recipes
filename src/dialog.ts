import type {ComponentProps, ComponentType, SvelteComponentTyped} from "svelte";

export interface DialogProps {
	onClose?: (button?: string) => void,
}

export function showDialog<
	Component extends SvelteComponentTyped,
>(
	Dialog: Component extends SvelteComponentTyped<infer _ extends DialogProps>
		? ComponentType<Component>
		: never,
	props: Omit<ComponentProps<Component>, keyof DialogProps>,
): Promise<string> {
	return new Promise((resolve, reject) => {
		let dialog = new Dialog({
			target: document.body,
			intro: true,
			props: {
				...props,
				onClose(button?: string) {
					dialog.$destroy();
					if (button === undefined) {
						reject("cancelled");
					} else {
						resolve(button);
					}
				},
			},
		});
	});
}
