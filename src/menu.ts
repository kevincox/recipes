import * as svelte from "svelte";

const context = Symbol("Menu");

export class MenuContext {
	constructor(
		readonly handleClose: (success: boolean) => void,
	) {}
}

export function getMenuContext(): MenuContext | undefined {
	return svelte.getContext(context);
}

export function setMenuContext(menuContext: MenuContext): void {
	svelte.setContext(context, menuContext);
}
