interface Fact {
	name: string,
	conversion: number,
	aliases: string,
}

const FACTS: Record<string, Record<string, Fact>> = {
	unknown: {},

	mass: {
		kg: {
			name: "killogram",
			conversion: 1000,
			aliases: "kilogram,kilograms",
		},
		g: {
			name: "gram",
			conversion: 1,
			aliases: "gram,grams",
		},
		mg: {
			name: "milligram",
			conversion: 0.001,
			aliases: "milligram,milligrams",
		},

		lb: {
			name: "pounds",
			conversion: 453.592,
			aliases: "lbs,pounds,pounds",
		},
		oz: {
			name: "ounces",
			conversion: 28.3495,
			aliases: "ounce,ounces",
		},
	},
	volume: {
		l: {
			name: "litre",
			conversion: 1,
			aliases: "litre,liter,litres,liters",
		},
		ml: {
			name: "millilitre",
			conversion: 0.001,
			aliases: "millilitre,milliliter,millilitres,milliliters",
		},

		cup: {
			name: "cups",
			conversion: 1/4,
			aliases: "cups,c",
		},
		"fl. oz.": {
			name: "fluid ounces",
			conversion: 0.033814,
			aliases: "floz,fluidounce,fluidounces",
		},
	},
};

export default class Unit {
	constructor(
		private readonly type: string,
		readonly unit: string,
		private readonly fact: Fact,
	) {}

	static find(name: string): Unit {
		let canonicalized = name.replace(/[^\w]+/g, "").toLowerCase();
		let unit = LOOKUP[canonicalized];
		if (unit) return unit;
		return new Unit("unknown", name, {name, conversion: 1, aliases: ""});
	}

	name(): string {
		return this.fact.name;
	}

	conversion(to: Unit): (amount: number) => number {
		return (amount) => amount * this.fact.conversion / to.fact.conversion;
	}

	availableConversions(): Unit[] {
		return Object.keys(FACTS[this.type] || {})
			.filter(unit => unit != this.unit)
			.map(unit => LOOKUP[unit]!);
	}

	toString(): string {
		return this.unit;
	}
}

const LOOKUP: Record<string, Unit> = {};

for (const [type, facts] of Object.entries(FACTS)) {
	for (const [unit, fact] of Object.entries(facts)) {
		let obj = new Unit(type, unit, fact);
		LOOKUP[unit] = obj;
		for (const alias of fact.aliases.split(",")) {
			LOOKUP[alias] = obj;
		}
	}
}
