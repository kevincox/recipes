import { type RecipeId,urlEncodeRecipeId } from "./db";

export function privateRecipeUrl(id: RecipeId): string {
	return `/private/${urlEncodeRecipeId(id)}`;
}
