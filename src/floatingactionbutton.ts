import type {ActionReturn} from "svelte/types/runtime/action";

import css from "./floatingactionbutton.module.scss";
import {ripple} from "./ripple";

export function floatingActionButton(node: HTMLElement): ActionReturn {
	node.classList.add(css.fab);
	return ripple(node);
}
