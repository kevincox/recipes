{
	nixpkgs ? import <nixpkgs> {},
	napalm ? nixpkgs.pkgs.callPackage (
		fetchTarball "https://github.com/nix-community/napalm/archive/master.tar.gz"
	) {},
}: with nixpkgs; let
in rec {
	npm = napalm.buildPackage
		(pkgs.nix-gitignore.gitignoreSource
			[
				"*"
				"!package.json"
				"!package-lock.json"
			]
			../recipes)
		{
			customPatchPackages = {
				# Version is not required. When it is not specified it
				# applies override to all packages with that name.
				"node-gyp-builder" = pkgs: prev: { };
			};
		};

	www = stdenv.mkDerivation {
		name = "recipes-www";

		src = pkgs.nix-gitignore.gitignoreSource
			[
				"*.nix"
				"*.yml"
				"renovate.json"
			]
			./.;

		buildInputs = with pkgs; [ nodejs ];

		buildPhase = ''
			ln -s ${npm}/_napalm-install/node_modules .
			npm run build
		'';

		doCheck = true;
		checkPhase = ''
			npm run check
			npm run lint -- --max-warnings 0
		'';

		installPhase = ''
			mv dist $out
		'';
	};
}
