import { svelte } from "@sveltejs/vite-plugin-svelte";
import { defineConfig } from "vite";

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [svelte()],
	build: {
		rollupOptions: {
			input: {
				main: "index.html",
				sw: "sw.ts",
			},
			output: {
				entryFileNames(chunkInfo) {
					if (chunkInfo.name === "main") {
						// Has a facade so still hash the name.
						return "a/[name]-[hash].js";
					}

					return "[name].js";
				},
			},
		},

		assetsDir: "a",
		sourcemap: true,
		minify: "terser",
	},
});
