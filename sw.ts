/// <reference lib="webworker" />
declare const self: ServiceWorkerGlobalScope;

const DEV_ASSETS = /^\/(@vite|.routify|node_modules|src)\//;

self.onfetch = e => {
	let req = e.request;
	let url = new URL(req.url);

	if (process.env.NODE_ENV == "development" && url.pathname.match(DEV_ASSETS)) {
		return;
	}

	if (url.origin != location.origin) return;
	if (url.pathname.startsWith("/a/")) return;

	e.respondWith(fetch("/", {
		method: req.method,
		headers: req.headers,
		referrer: req.referrer,
	}));
};

export {};
