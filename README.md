# recipes.kevincox.ca

A bring-your-own storage recipe website.

Key features:
- Unit conversions.
- Quantity adjustments.
- You own your data.

Missing features:
- Public recipes.
- Shared recipes.

## Development

```sh
npm run dev # Start dev server.
```
